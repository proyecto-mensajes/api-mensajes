from typing import Optional

from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, WebSocket
from pydantic import BaseModel

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=["DELETE", "GET", "POST", "PUT"],
    allow_headers=["*"],
)
class Usuario(BaseModel):
    id: Optional[int] = None
    nombre: str
    mensaje: str = None

@app.post("/usuario/")
def crear_mensaje(usuario: Usuario):
    """
    Este endpoint crea un nuevo usuario en el sistema
    """
    print("Listo para recibir el primer mensaje")
    print(usuario.nombre, usuario.mensaje)
    
    return {
        "codigo": "Peticion Exitosa",
        "mensaje": "Mensaje enviado "
    }

@app.websocket("/mensaje/{nombre}/{msg}/ws")
async def websocket_endpoint(websocket: WebSocket, nombre: str, msg: str):
    await websocket.accept()
    print("WS conectado nuevo")
    while True:
        data = await websocket.receive_text()
        await websocket.send_text(f"{nombre} muchas gracias por comunicarte con nosotros")